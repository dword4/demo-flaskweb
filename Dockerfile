FROM alpine:3.9

RUN apk update

RUN mkdir -p /demo-flaskweb

WORKDIR /demo-flaskweb
RUN apk add git
RUN apk add py-pip
COPY . /demo-flaskweb
RUN pip install -r requirements.txt

# set timezone
RUN apk add tzdata
ENV TZ=America/New_York

ENV FLASK_APP=app.py
EXPOSE 5000
CMD ["flask", "run", "--host=0.0.0.0"]
