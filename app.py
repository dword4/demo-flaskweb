from flask import Flask, render_template

import os
import socket

APP_VERSION = "0.3"

app = Flask(__name__)
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("5000"), threaded=True)

@app.route("/")
def index():
    title = "demo-flaskweb"
    host = socket.gethostname() 
    ip = socket.gethostbyname(host)
    return render_template('index.html', title=title, ip=ip, host=host, app_ver=APP_VERSION)
